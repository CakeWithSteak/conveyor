#pragma once

//If our app was not ran from the console, this function will pause it to give time to the user to read
// the output before the window closes.
//Only used on Windows, on other platforms it does nothing.
void pause();