#include "pause.h"

#ifndef _WIN32

//Do nothing on non-Windows systems
void pause() {}

#else
#include <Windows.h>

//Checks if the console window we are using was created by our process
bool isConsoleWindowOurs() {
    HWND win = GetConsoleWindow();
    DWORD creatorID;
    GetWindowThreadProcessId(win, &creatorID);
    return (creatorID == GetCurrentProcessId());
}

void pause() {
    if(isConsoleWindowOurs())
        system("pause");
}

#endif