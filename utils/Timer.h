#pragma once
#include <chrono>

//Helper class for benchmarking

using std::chrono::microseconds;
class Timer {
    using clock = std::chrono::high_resolution_clock;

    clock::time_point t0;

public:
    Timer() {reset();}

    //Resets the Timer and returns the elapsed time
    microseconds reset() {
        auto temp = std::chrono::duration_cast<microseconds>(clock::now() - t0);
        t0 = clock::now();
        return temp;
    }

    //Returns the elapsed time without changing the Timer
    microseconds get() {
        return std::chrono::duration_cast<microseconds>(clock::now() - t0);
    }
};


