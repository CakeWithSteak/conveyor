R"(

#version 330 core

in vec2 vTexCoords;

uniform sampler2D image;

void main() {
    gl_FragColor = texture(image, vTexCoords);
    //gl_FragColor = vec4(1,0,1,1);
    //gl_FragColor = vec4(vTexCoords, 0, 1);
}

)"