#include "SimulationRenderer.h"
#include "../utils/Timer.h"
#include <cmath>
#include <chrono>
#include <iostream>

Simulation& SimulationRenderer::simulation() {
    return sim;
}


void SimulationRenderer::render(double stepSize) {
    sim.step(stepSize);

    ImageBuffer& renderImg = image();
    const double xTransform = image().getWidth() / static_cast<double>(field().getCols());
    const double yTransform = image().getHeight() / static_cast<double>(field().getRows());

    for(int64_t i{0};i < sim.getNumObjects();++i) {
        //Since it would be inefficient to have a vector for every pixel in the image, each vector will affect a
        // rectangle of pixels instead. Thus the Simulation and the image use two different coordinate systems,
        // which we will handle using xTransform and yTransform.

        const PhysicsObject& obj = sim.getObject(i);
        int64_t x = static_cast<int64_t>(obj.pos.x * xTransform);
        int64_t y = static_cast<int64_t>(obj.pos.y * yTransform);

        renderImg[i] = originalImg(x, y);
    }

    refresh();
    DynamicImageRenderer::render();
}

VectorField& SimulationRenderer::field() {
    return sim.field;
}

