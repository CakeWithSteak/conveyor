#pragma once
#include "DynamicImageRenderer.h"
#include "../Simulation/VectorField.h"
#include "../Simulation/Simulation.h"

//A renderer which renders a simulation
//Upon calling render(), it will automatically advance the simulation, and render the results
//The vector field used can be smaller than the image - in this case the vector field will be stretched to match

class SimulationRenderer : public DynamicImageRenderer {
    Simulation sim;

public:
    ImageBuffer originalImg;

    SimulationRenderer(Simulation& sim, int64_t ibWidth, int64_t ibHeight, RGB bgColor) :
        DynamicImageRenderer(ImageBuffer(ibWidth,ibHeight, bgColor)), sim(sim), originalImg(image()) {}

    SimulationRenderer(Simulation&& sim, int64_t ibWidth, int64_t ibHeight, RGB bgColor) :
        DynamicImageRenderer(ImageBuffer(ibWidth,ibHeight, bgColor)), sim(std::move(sim)), originalImg(image()) {}

    SimulationRenderer(int64_t vfWidth, int64_t vfHeight, int64_t ibWidth, int64_t ibHeight, RGB bgColor) :
        DynamicImageRenderer(ImageBuffer(ibWidth, ibHeight, bgColor)), sim(VectorField(vfHeight, vfWidth)), originalImg(image()) {}

    SimulationRenderer(Simulation&& sim, ImageBuffer&& original) :
        DynamicImageRenderer(original),  sim(std::move(sim)), originalImg(std::move(original)) {}

    Simulation& simulation();

    VectorField& field();

    void render(double stepSize = 1.0);
};


