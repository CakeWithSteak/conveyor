#pragma once
#include <vector>
#include <utility>
#include <cstdint>

//Helper classes for storing and handling images
//Pixels are stored in row-major order as bytes

struct RGB {
    unsigned char R;
    unsigned char G;
    unsigned char B;
};

class ImageBuffer {
    int64_t width{0};
    int64_t height{0};
    std::vector<RGB> data;
public:
    ImageBuffer(int64_t width, int64_t height, RGB color) : width(width), height(height), data(width * height, color) {}

    ImageBuffer(std::vector<RGB>&& data, int64_t width, int64_t height) : width(width), height(height), data(std::move(data)) {}

    int64_t getWidth() const {
        return width;
    }

    int64_t getHeight() const {
        return height;
    }

    //Copies data from a raw float array
    ImageBuffer(const unsigned char* buffer, int64_t width, int64_t height) : width(width), height(height) {
        const RGB* rgbBuffer = reinterpret_cast<const RGB*>(buffer);
        data.assign(rgbBuffer, rgbBuffer + (width * height));
    }

    ImageBuffer(ImageBuffer& ib) = default;
    ImageBuffer(ImageBuffer&& ib) noexcept : width(ib.width), height(ib.height), data(std::move(ib.data)) {
    }

    RGB& operator()(int64_t x, int64_t y) {
        return data[y * width + x];
    }

    const RGB& operator()(int64_t x, int64_t y) const {
        return data[y * width + x];
    }

    //Raw element access
    RGB& operator[](int64_t i) {
        return data[i];
    }

    const RGB& operator[](int64_t i) const {
        return data[i];
    }

    //Returns the internal buffer of the image as an array of bytes with size (width * height * 3)
    unsigned char* getRawData() {
        return reinterpret_cast<unsigned char*>(data.data());
    }
};


