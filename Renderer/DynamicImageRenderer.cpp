#include "DynamicImageRenderer.h"
#include "../utils/thirdparty/glad.h"
#include "shaders.h"
#include <stdexcept>
#include <string>

float data[] = {
        //XY position and UV coordinates

        -1,  1,  0, 0,  //top left
        -1, -1,  0, 1, //bottom left
         1,  1,  1, 0, //top right

         1,  1,   1, 0, //top right
        -1, -1,   0, 1, //bottom left
         1, -1,   1, 1, //bottom right
};

void DynamicImageRenderer::init() {
    //Init VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    //Init VBO
    unsigned int VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);

    //Position attribute
    glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(float) * 4, nullptr);
    glEnableVertexAttribArray(0);

    //Texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(float) * 4, (void*)(sizeof(float) * 2));
    glEnableVertexAttribArray(1);

    initTexture();
    initShaders();
}

void DynamicImageRenderer::initTexture() {
    glGenTextures(1, &texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.getWidth(), img.getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, img.getRawData());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void DynamicImageRenderer::initShaders() {
    shaderProg = glCreateProgram();

    unsigned int fragShader, vertShader;

    vertShader = glCreateShader(GL_VERTEX_SHADER);
    auto vertSrcPtr = vertexShaderSrc.c_str();
    glShaderSource(vertShader, 1, &vertSrcPtr, nullptr);
    glCompileShader(vertShader);

    char infoLog[512];
    int success;
    glGetShaderiv(vertShader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vertShader, sizeof(infoLog), nullptr, infoLog);
        std::string str(infoLog);
        throw std::runtime_error(("Failed to load vertex shader: " + str).c_str());
    }

    fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    auto fragSrcPtr = fragShaderSrc.c_str();
    glShaderSource(fragShader, 1, &fragSrcPtr, nullptr);
    glCompileShader(fragShader);

    glGetShaderiv(fragShader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fragShader, sizeof(infoLog), nullptr, infoLog);
        std::string str(infoLog);
        throw std::runtime_error(("Failed to load fragment shader: " + str).c_str());
    }

    glAttachShader(shaderProg, vertShader);
    glAttachShader(shaderProg, fragShader);

    glLinkProgram(shaderProg);

    glDeleteShader(fragShader);
    glDeleteShader(vertShader);

    glGetProgramiv(shaderProg, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(fragShader, sizeof(infoLog), nullptr, infoLog);
        std::string str(infoLog);
        throw std::runtime_error(("Failed to link shader program: " + str).c_str());
    }

    glUseProgram(shaderProg);
    glUniform1i(glGetUniformLocation(shaderProg, "image"), 0);
}


void DynamicImageRenderer::render() {
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

void DynamicImageRenderer::refresh() {
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, img.getWidth(), img.getHeight(), GL_RGB, GL_UNSIGNED_BYTE, img.getRawData());
}