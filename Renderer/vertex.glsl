R"(
//The shader code is wrapped in a c++ raw string so that it can be simply #included in a source file
#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoords;

out vec2 vTexCoords;

void main() {
    gl_Position = vec4(position, 0, 1);

    vTexCoords = texCoords;
}

)"