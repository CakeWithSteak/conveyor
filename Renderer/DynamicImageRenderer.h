#pragma once
#include "ImageBuffer.h"

//A class for rendering a constantly changing image (... that is, video...?)

class DynamicImageRenderer {
    ImageBuffer img;
    unsigned int texture{0};
    unsigned int VAO{0};
    unsigned int shaderProg{0};

    void init();
    void initTexture();
    void initShaders();
public:
    explicit DynamicImageRenderer(ImageBuffer& img) : img(img) { init(); }
    explicit DynamicImageRenderer(ImageBuffer&& img) : img(std::move(img)) { init(); }

    //Don't let the renderer be copied
    DynamicImageRenderer(DynamicImageRenderer& other) = delete;

    //Access to the image
    //Note that after writing to the image you should call refresh() to upload the changes to the GPU
    inline ImageBuffer& image() {
        return img;
    }


    void refresh();

    //Renders the image to the screen. Buffers are not swapped, you can render more stuff afterwards
    //Assumes that an OpenGL context is active
    void render();

};


