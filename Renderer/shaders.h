#pragma once
#include <string>


//This file contains all shaders as strings
//I want to have a self-contained executable at the end, so shaders can't be loaded from separate files.
//Even though this method is kind of ugly, it gets the job done perfectly well.

std::string vertexShaderSrc {
    #include "vertex.glsl"
};

std::string fragShaderSrc {
    #include "frag.glsl"
};
