#include <iostream>
#include <string>
#include "Renderer/SimulationRenderer.h"
#include "utils/thirdparty/glad.h"
#include <GLFW/glfw3.h>
#include "Operations/OperationParser.h"
#include "utils/Timer.h"
#include "utils/pause.h"
#include <algorithm>

#define STB_IMAGE_IMPLEMENTATION
#include "utils/thirdparty/stb_image.h"

GLFWwindow* setupWindow(int width, int height) {
    glfwSetErrorCallback([]([[maybe_unused]] int unused, const char* errstr) {
        std::cerr << "GLFW error: " << errstr << std::endl;
    });

    if(!glfwInit()) {
        throw std::runtime_error("Failed to initialize GLFW");
    }

    std::atexit(glfwTerminate);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(width, height, "conveyor", nullptr, nullptr);
    if(!window) {
        throw std::runtime_error("Failed to open window");
    }

    glfwMakeContextCurrent(window);

    //Ensure that the context has been made current
    if(!glfwGetCurrentContext()) {
        throw std::runtime_error("Failed to setup OpenGL context");
    }

    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
        throw std::runtime_error("Failed to initialize GLAD");
    }

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);

    //Sync with the monitor's refresh rate
    glfwSwapInterval(1);

    glViewport(0, 0, width, height);
    glClearColor(0, 0, 0, 0);

    glfwSetFramebufferSizeCallback(window, []([[maybe_unused]] GLFWwindow* window, int width, int height) {
       glViewport(0, 0, width, height);
    });
    return window;
}

Simulation initSimulation(Operations::OperationTree& eq, int64_t width, int64_t height, double vfSizeMultiplier, double maxX, double maxY)
{
    int64_t vfHeight = std::llround(vfSizeMultiplier * height);
    int64_t vfWidth = std::llround(vfSizeMultiplier * width);
    VectorField vf(vfHeight, vfWidth);
    std::vector<PhysicsObject> objs(width * height);

    //Populate the vector field
    for(int64_t x{0};x < vfWidth;++x) {
        for(int64_t y{0};y < vfHeight;++y) {
            //Our coordinate system's y-axis increases downwards a positive angle would result in clockwise rotation.
            //To counter this we multiply the angle with -1, making ccw the positive rotational direction
           vf(x, y) = Vector::fromPolar(-eq.exec({{'x', (static_cast<double>(x)/vfWidth)*maxX},
                                                  {'y', (static_cast<double>(y)/vfHeight)*maxY}}), 1);
        }
    }


    //Create the physical objects
    const double xTransform = static_cast<double>(width) / vfWidth;  //Used to translate to the vf's coordinate system
    const double yTransform = static_cast<double>(height) / vfHeight;

    for(int64_t x{0};x < width;++x) {
        for(int64_t y{0};y < height;++y) {
            objs[y * width + x] = PhysicsObject{{x / xTransform, y / yTransform}};
        }
    }

    return Simulation(std::move(objs), std::move(vf), 1);
}

int main(int argc, char** argv) {
    using namespace std::chrono_literals;

    std::string filename;
    if(argc >= 2) {
        filename = argv[1];
    } else {
        std::cout << "Enter an image to load: ";
        std::getline(std::cin, filename);
    }

    int imgW;
    int imgH;

    unsigned char* image = stbi_load(filename.c_str(), &imgW, &imgH, nullptr, 3);

    if(!image) {
        std::cout << "Couldn't load image.\n";
        pause();
        return -1;
    }

    ImageBuffer ib(image, imgW, imgH);

    stbi_image_free(image);

    std::string eqStr;
    if(argc >= 3) {
        eqStr = argv[2];
    } else {
        while(eqStr.empty()) {
            std::cout << "Enter an equation: ";
            std::getline(std::cin, eqStr);
        }
    }

    double maxX{20}, maxY{20};
    try {
        if (argc >= 5) {
            maxX = std::stod(argv[3]);
            maxY = std::stod(argv[4]);
        } else {
            std::cout << "X maximum (20):";
            std::string temp;
            std::getline(std::cin, temp);
            if (!temp.empty())
                maxX = std::stod(temp);

            std::cout << "Y maximum (20):";
            std::getline(std::cin, temp);
            if (!temp.empty())
                maxY = std::stod(temp);
        }
    } catch(std::invalid_argument& err) {
        std::cout << "Error: X and Y maximums must be decimal numbers.\n";
        pause();
        return -1;
    }

    if(maxX <= 0 || maxY <= 0) {
        std::cout << "Error: X and Y maximum should be greater than zero.\n";
        pause();
        return -1;
    }

    double vfSizeMultiplier{1};
    try {
        if (argc >= 6) {
            vfSizeMultiplier = std::stod(argv[5]);
        } else {
            std::cout << "Vector field size multiplier (1):";
            std::string temp;
            std::getline(std::cin, temp);
            if (!temp.empty())
                vfSizeMultiplier = std::stod(temp);
        }
    } catch(std::invalid_argument& err) {
        std::cout << "Error: Vector field size multiplier must be a decimal number.\n";
        pause();
        return -1;
    }

    if(vfSizeMultiplier <= 0) {
        std::cout << "Error: Vector field size multiplier should be greater than zero.\n";
        pause();
        return -1;
    }

    Operations::OperationTree eq;
    try {
        eq = Operations::parseEquation(eqStr);
    } catch(std::exception& err) {
        std::cout << "Invalid equation: " << err.what() << "\n";
        pause();
        return -1;
    }

    GLFWwindow* window{nullptr};
    try {
        window = setupWindow(imgW, imgH);
    } catch(std::exception& err) {
        std::cout << "Failed to set up graphics: " << err.what() << "\n";
        pause();
        return -1;
    }

    Simulation sim;
    try {
        sim = initSimulation(eq, imgW, imgH, vfSizeMultiplier, maxX, maxY);
    } catch(std::exception& err) {
        std::cout << "Failed to initialize simulation: " << err.what() << "\n";
        pause();
        return -1;
    }

    //Catch any exceptions that happen during rendering/simulating
    try {

        SimulationRenderer renderer(std::move(sim), std::move(ib));

        //baseSpeed determines how many steps are done in every second in total
        double baseSpeed{42};
        constexpr double maxBaseSpeed{500};
        constexpr double speedStep{0.4};
        //A serious processing delay could cause a very big deltatime, which would lead to issues
        constexpr double maxDeltaTime{1};

        Timer timer;

        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT);

            double deltaTime = timer.reset().count() / (1000.0 * 1000.0);
            deltaTime = std::min(deltaTime, maxDeltaTime);
            //By speeding up on big fields and slowing down on small fields we get approximately the same speeds for
            // the same image at any field size
            //For example on a field that is 10 times smaller the simulation will run 10 times slower to compensate
            double effectiveStepSpeed = baseSpeed * deltaTime * vfSizeMultiplier;
            renderer.render(effectiveStepSpeed);

            if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
                baseSpeed += speedStep;
                baseSpeed = std::min(baseSpeed, maxBaseSpeed);
            }
            if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
                baseSpeed -= speedStep;
                baseSpeed = std::max(baseSpeed, 0.0);
            }

            glfwSwapBuffers(window);
            glfwPollEvents();
        }

    } catch(std::exception& err) {
        std::cout << "Fatal error: " << err.what() << "\n";
        pause();
        return -1;
    }

    return 0;
}
