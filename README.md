conveyor
========

Conveyor is a small app that takes an image and an equation, and uses the equation to manipulate the image.

Explanation
-----------

Conveyor works the following way:
1. Conveyor loads the image
2. The equation is evaluated for each pixel (by default, see below) and the results are used to create a vector field.
   * Each result is interpreted as an angle in radians which defines the direction of the vector.
   * For example, given the equation `(x+y-4)` at `(4, 3)` we get a result of `3` which leads to a vector pointing to the left
3. For each pixel of the image a point-like object is generated above the pixel. Each object remembers its original position.
4. The simulation starts. At each frame:
   1. The objects move above the image and are accelerated by the vectors underneath them
   2. Each objects looks at what color is "below" it on the original image
   3. Each object sets the color of the display at its original location (that it remembered in step 2) to the color below it right now.  
    (Note that the original image isn't changed.)
    

Options
-------

#### Image
The (relative or absolute) path to the image that should be used.  
On windows you can also just drag the image over the conveyor executable file and it will be automatically loaded.

#### Equation
The equation used to create the vector field.  
The following operations are recognized:

* Addition: `x+y`
* Subtraction: `x-y`
* Multiplication: `x*y` or just `xy`
* Division: `x/y`
* Integer modulo: `x%y` (note that here x and y will be rounded to the nearest integer)
* Binary XOR: `x^y`
* Binary AND: `x&y`
* Binary OR: `x|y`
* Negation `-(x*y)`
* Binary NOT `~(x*y)`

Note that on binary operations the operands will be rounded and their absolute values will be used.

Conveyor also supports the following functions:
* Exponentiation `pow(x,y)`
* Logarithms:
  * `log2(x)`
  * `log10(x)`
  * `ln(x)`
* Some trig functions:
  * `sin(x)`
  * `cos(x)`
  * `tan(x)`
* Absolute value `abs(x-y)`
* Floor and ceil: `floor(xy)`, `ceil(y/2)`
* Round `round(x+y/2)`
* Square root: `sqrt(xy)`

The equation has to have a valid value everywhere on the image (in the range `[0, xmax]` for x and `[0, ymax]` for y, see below) otherwise the vector field can not be created.

#### X and Y maximum
The values of x and y at the bottom right corner of the image.  
This defines the range the equation will be evaluated on.  
Defaults to 20 if not provided.

#### Vector field size multiplier

By default the vector field has the same size as the image, every pixel has its own vector.
However, on big images this can consume a lot of memory, and can get very slow.  
The size multiplier can be used to reduce the size of the field to save memory and speed up the simulation.
For example at the value of 0.5 only every 2x2 square of pixels has its own vector, reducing the number of vectors needed by 75%.  
Defaults to 1 if not provided.

Usage
-----
Conveyor can be used both from your GUI or from the command line.  
If started from the GUI, you will have to enter the required options (see above), or press enter to choose the default value shown in parentheses.  
From the command line you will have to specify the options as follows:
```
conveyor [image path] [equation] [x maximum] [y maximum] [size multiplier]
```
You can leave out the last three arguments, in which case they use their default values.