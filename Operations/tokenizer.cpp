#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include <exception>
#include <stdexcept>
#include <cctype>
#include "tokenizer.h"

namespace Operations {
    TokenList tokenize(const std::string& eq) {
        TokenList res;
        for (size_t i{0}; i < eq.length(); ++i) {
            char ch = eq[i];

            if (ch == '(') {
                res.push_back(Token(TokenType::PAREN_OPEN, '('));
            } else if (ch == ')') {
                res.push_back(Token(TokenType::PAREN_CLOSE, ')'));
            } else if (isalpha(ch)) {
                std::string funcName;
                bool loopFinished{false};
                for (size_t k{i}; k < eq.length() && !loopFinished; ++k) {
                    char x{eq[k]};
                    if (x == '(') {
                        if (std::find(validFunctions.cbegin(), validFunctions.cend(), funcName) !=
                            validFunctions.cend()) {
                            res.push_back(Token(TokenType::FUNCTION, funcName));
                            i = k - 1;
                            loopFinished = true;
                        } else {
                            throw std::invalid_argument((ERR_UNKNOWN_FUNCTION + "\"" + funcName + "\"").c_str());
                        }
                    } else if (isalpha(x) || isdigit(x)) {
                        funcName.push_back(x);
                    } else {
                        res.push_back(Token(TokenType::VARIABLE, ch));
                        loopFinished = true;
                    }
                }

                if (!loopFinished) {
                    res.push_back(Token(TokenType::VARIABLE, ch));
                }
            } else if (ch == ',') {
                res.push_back(Token(TokenType::COMMA, ch));
            } else if (isdigit(ch) || ch == '.') {
                std::string num;
                bool hadDot{false};
                do {
                    if (ch == '.') {
                        if (hadDot)
                            throw std::invalid_argument(ERR_INVALID_NUMBER + std::to_string(i) + '\n');
                        else hadDot = true;
                    }
                    num.push_back(ch);

                    ++i;
                    if (i == eq.length()) break;
                    ch = eq[i];
                } while (isdigit(ch) || ch == '.');
                --i;

                res.push_back(Token(TokenType::NUMBER_LITERAL, num));
            } else if (validOperators.find(ch) != std::string::npos) {
                res.push_back(Token(TokenType::OPERATOR, ch));
            } else if (ch != ' ' && ch != '\t') {
                throw std::invalid_argument(ERR_UNEXPECTED_TOKEN + std::to_string(i) + '\n');
            }
        }//for
        return res;
    }

}