#pragma once

#include <string>
#include <vector>
#include <list>

namespace Operations {

    enum class TokenType {
        INVALID,
        PAREN_OPEN,
        PAREN_CLOSE,
        VARIABLE,
        NUMBER_LITERAL,
        OPERATOR,
        COMMA,
        FUNCTION
    };

    struct Token {
        TokenType type{TokenType::INVALID};
        std::string value;

        Token(TokenType ptype, std::string& pvalue) : type{ptype}, value{pvalue} {}

        Token(TokenType ptype, char *pvalue) : type{ptype}, value{pvalue} {}

        Token(TokenType ptype, char pvalue) : type{ptype}, value{pvalue} {}

        friend bool operator==(const Token& x, const TokenType& y) { return x.type == y; }
    };

    using TokenList = std::list<Token>;

    const std::string ERR_INVALID_NUMBER{"Invalid number literal at: "};
    const std::string ERR_UNEXPECTED_TOKEN{"Unexpected token at: "};
    const std::string ERR_UNKNOWN_FUNCTION{"Unknown function: "};

    const std::string validOperators{"+-*/%^~|&"};
    const std::vector<std::string> validFunctions{"pow", "power", "log2", "log10", "ln", "sin", "cos", "tan", "mod",
                                                  "abs", "floor", "ceil", "round", "sqrt"};

    TokenList tokenize(const std::string& eq);

}