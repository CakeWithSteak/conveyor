#include "Operations.h"
#include <cmath>
#include <cstdint>
#include <cstring>

namespace Operations {

inline uint64_t toUint64(long double x)
{
    return static_cast<uint64_t>(std::abs(std::round(x)));
}

long double OperationTree::exec(const VariableContainer &variables) const
{
    if (!root)
    {
        throw std::logic_error(ERR_EMPTY_OPTREE);
    }
    return root->exec(variables);
}

long double OperationTree::exec() const
{
    VariableContainer temp;
    return exec(temp);
}

IOperation* OperationTree::swapRootOp(IOperation *newOp)
{
    IOperation *oldOp{ root.release() };
    root.reset(newOp);
    return oldOp;
}

long double UnaryNegative::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return -(x->exec(variables));
}

long double BinaryNOT::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return static_cast<size_t>(~toUint64(x->exec(variables)));
}

long double Addition::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return x->exec(variables) + y->exec(variables);
}

long double Subtraction::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return x->exec(variables) - y->exec(variables);
}

long double Multiplication::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return x->exec(variables) * y->exec(variables);
}

long double Division::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    long double valueY{ y->exec(variables) };
    if (valueY == 0)
        throw std::invalid_argument(ERR_INVALID_PARAMETERS + "Division by zero");
    return x->exec(variables) / valueY;
}

long double Modulo::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    int64_t valueY{ static_cast<int64_t>(std::round(y->exec(variables))) };
    if (valueY == 0)
        throw std::invalid_argument(ERR_INVALID_PARAMETERS + "Modulo by zero");
    return static_cast<int64_t>(std::round(x->exec(variables))) % valueY;
}

long double BinaryAND::exec(const VariableContainer &variables) const
{
    if (!x ||!y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return toUint64(x->exec(variables)) & toUint64(y->exec(variables));
}

long double BinaryOR::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return toUint64(x->exec(variables)) | toUint64(y->exec(variables));
}

long double BinaryXOR::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return toUint64(x->exec(variables)) ^ toUint64(y->exec(variables));
}

long double Exponentiation::exec(const VariableContainer &variables) const
{
    if (!x || !y)
        throw std::logic_error(ERR_INVALID_OPTREE);
    long double xval = x->exec(variables);
    long double yval = y->exec(variables);
    long double res{std::pow(xval, yval)};
    if(std::isnan(res) || std::isinf(res)) {
        throw std::invalid_argument(ERR_INVALID_PARAMETERS + "pow(" + std::to_string(xval) + ", " + std::to_string(yval) + ")");
    }
    return res;
}

long double Log2::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    long double valueX{ x->exec(variables) };
    if (valueX <= 0)
        throw std::invalid_argument(ERR_INVALID_PARAMETERS + "Log2 " + std::to_string(valueX));
    return std::log2(valueX);
}

long double Log10::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    long double valueX{ x->exec(variables) };
    if (valueX <= 0)
        throw std::invalid_argument(ERR_INVALID_PARAMETERS + "Log10 " + std::to_string(valueX));
    return std::log10(valueX);
}

long double NaturalLogarithm::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    long double valueX{ x->exec(variables) };
    if (valueX <= 0)
        throw std::invalid_argument(ERR_INVALID_PARAMETERS + "Ln " + std::to_string(valueX));
    return std::log(valueX);
}

long double Sin::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return std::sin(x->exec(variables));
}

long double Cos::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return std::cos(x->exec(variables));
}

long double Tan::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return std::tan(x->exec(variables));
}

long double Abs::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return std::abs(x->exec(variables));
}

long double Ceil::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return std::ceil(x->exec(variables));
}

long double Floor::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return std::floor(x->exec(variables));
}

long double Round::exec(const VariableContainer &variables) const
{
    if (!x)
        throw std::logic_error(ERR_INVALID_OPTREE);
    return std::round(x->exec(variables));
}

long double SquareRoot::exec(const VariableContainer &variables) const
{
	if(!x)
		throw std::logic_error(ERR_INVALID_OPTREE);
	long double a{ x->exec(variables) };
	if (a < 0)
		throw std::invalid_argument(ERR_INVALID_PARAMETERS + "Sqrt " + std::to_string(a));
	return std::sqrt(a);
}

}