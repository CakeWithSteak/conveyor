#pragma once
#include <map>
#include <memory>
#include <vector>
#include <string>

//Some tools for parsing and evaluating operations

//This code is from a previous project of mine. I know that there are external libraries that are faster/better/more reliable
// than this, but since performance is not a big concern during initialization, and I already had this written, so
// I thought I might as well use it. It serves its purpose just fine.

namespace Operations {

    const std::string ERR_INVALID_OPTREE{"Invalid operation tree: could not execute a node on tree."};
    const std::string ERR_EMPTY_OPTREE{"The OperationTree executed was empty."};
    const std::string ERR_UNDEFINED_VARIABLE{"A variable in the OperationTree was not defined: "};
    const std::string ERR_INVALID_PARAMETERS{"Invalid parameters in equation: "};

    using VariableContainer = std::map<char, long double>;

    //Base class for all operations
    class IOperation {
    public:
        virtual ~IOperation() = default;

        virtual long double exec(const VariableContainer& variables) const = 0;
    };

    //Container for IOperation
    class OperationTree {
        std::unique_ptr<IOperation> root{nullptr};
    public:
        long double exec() const;

        long double exec(const VariableContainer& variables) const;

        IOperation *getRootOp() const { return root.get(); }

        IOperation *releaseRootOp() { return root.release(); }

        void setRootOp(IOperation *x) { root.reset(x); }

        IOperation *swapRootOp(IOperation *newOp);

        explicit operator bool() { return static_cast<bool>(root); }

        friend bool operator==(const OperationTree& x, const OperationTree& y) { return x.root == y.root; }

        OperationTree& operator=(OperationTree& x) = delete;

        OperationTree& operator=(OperationTree&& x) noexcept {
            root.reset(x.releaseRootOp());
            x.setRootOp(nullptr);
            return *this;
        }

        OperationTree() = default;

        OperationTree(OperationTree& x) = delete;

        OperationTree(OperationTree&& x) noexcept {
            this->root = std::move(x.root);
            x.setRootOp(nullptr);
        }

        explicit OperationTree(IOperation *op) : root{op} {}

    };

    class UnaryOperation : public IOperation {
    public:
        IOperation *x{nullptr};

        UnaryOperation() = default;

        explicit UnaryOperation(IOperation *px) : x{px} {}

        friend bool operator==(UnaryOperation& x, UnaryOperation& y) { return x.x == y.x; }

        UnaryOperation& operator=(UnaryOperation&& a) noexcept {
            this->x = a.x;
            a.x = nullptr;
            return *this;
        }

        virtual ~UnaryOperation() override { delete x; }
    };

    class BinaryOperation : public IOperation {
    public:
        IOperation *x{nullptr};
        IOperation *y{nullptr};

        BinaryOperation() = default;

        BinaryOperation(IOperation *px, IOperation *py) : x{px}, y{py} {}

        friend bool operator==(BinaryOperation& a, BinaryOperation& b) { return (a.x == b.x && a.y == b.y); }

        BinaryOperation& operator=(BinaryOperation&& a) noexcept {
            this->x = a.x;
            this->y = a.y;
            a.x = nullptr;
            a.y = nullptr;
            return *this;
        }

        virtual ~BinaryOperation() override {
            delete x;
            delete y;
        }
    };

    #define UnaryOperationCreator(className) \
    class className : public UnaryOperation\
    {\
    public:\
        virtual long double exec(const VariableContainer &variables) const override;\
        explicit className(IOperation *px) : UnaryOperation(px) {}\
    }

    #define BinaryOperationCreator(className) \
    class className : public BinaryOperation\
    {\
    public:\
        virtual long double exec(const VariableContainer &variables) const override;\
        className(IOperation *px,IOperation *py) : BinaryOperation(px,py)  {}\
    }

    class Literal : public IOperation {
    public:
        long double value;

        virtual long double exec([[maybe_unused]] const VariableContainer& variables) const override { return value; }

        explicit Literal(long double pvalue) : value{pvalue} {}

        friend bool operator==(Literal& x, Literal& y) { return x.value == y.value; }

        Literal& operator=(const Literal& x) {
            this->value = x.value;
            return *this;
        }
    };

    class Variable : public IOperation {
    public:
        char identifier;

        virtual long double exec(const VariableContainer& variables) const override {
            try {
                return variables.at(identifier);
            }
            catch (std::out_of_range&) {
                throw std::invalid_argument(ERR_UNDEFINED_VARIABLE + identifier);
            }
        }

        explicit Variable(char pidentifier) : identifier{pidentifier} {}

        friend bool operator==(Variable& x, Variable& y) { return x.identifier == y.identifier; }

        Variable& operator=(const Variable& x) {
            this->identifier = x.identifier;
            return *this;
        }
    };

    UnaryOperationCreator(UnaryNegative);
    UnaryOperationCreator(BinaryNOT);
    UnaryOperationCreator(Log2);
    UnaryOperationCreator(Log10);
    UnaryOperationCreator(NaturalLogarithm);
    UnaryOperationCreator(Sin);
    UnaryOperationCreator(Cos);
    UnaryOperationCreator(Tan);
    UnaryOperationCreator(Abs);
    UnaryOperationCreator(Floor);
    UnaryOperationCreator(Ceil);
    UnaryOperationCreator(Round);
    UnaryOperationCreator(SquareRoot);

    BinaryOperationCreator(Addition);
    BinaryOperationCreator(Subtraction);
    BinaryOperationCreator(Multiplication);
    BinaryOperationCreator(Division);
    BinaryOperationCreator(Modulo);
    BinaryOperationCreator(BinaryAND);
    BinaryOperationCreator(BinaryOR);
    BinaryOperationCreator(BinaryXOR);
    BinaryOperationCreator(Exponentiation);

}