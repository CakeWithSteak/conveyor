#include "OperationParser.h"
#include "Operations.h"
#include "tokenizer.h"
#include <string>
#include <algorithm>
#include <cassert>
#include <iterator>

/*
 * The parser works by iterating through the tokens in reverse, looking for tokens that match its state (see PEStates)
 * If no such tokens are found the parser "peels" a layer of parentheses off the equation and tries again.
 * If still no matching tokens are found the parser advances to the next state.
 * If a token is found the parser separates the operation's operands and recursively parses each of them, assembling a tree
 */

namespace Operations {

    using PEState = int;

//PEStates
    const PEState ADD_SUB{0};
    const PEState MUL_DIV{1};
    const PEState JUXTAPOS_MOD{2};
    const PEState FUN{3};
    const PEState UNARY{4};
    const PEState LITERAL{5};
    const PEState RESET{6};

    using std::next;
    using std::prev;


    template <typename It>
    IOperation* parseEquation(It tokBegin, It tokEnd);

    template <typename It>
    IOperation* _parseEquation(It tokBegin, It tokEnd);

    OperationTree parseEquation(TokenList&& tokens) {
        return OperationTree(parseEquation(tokens.cbegin(), tokens.cend()));
    }

    template <typename It>
    size_t countParens(It tokBegin, It tokEnd) {
        size_t openParens{0};
        size_t closeParens{0};
        for (auto it{tokBegin}; it != tokEnd; ++it) {
            if (it->type == TokenType::PAREN_OPEN) ++openParens;
            else if (it->type == TokenType::PAREN_CLOSE) ++closeParens;
        }

        return std::max(openParens, closeParens);
    }

    template<typename It>
    It findCorrespondingCloseParen(It it) {
        if (it->type != TokenType::PAREN_OPEN) throw parser_error(ERR_INVALID_FUNCTION + "Unknown");
        size_t parenLevel{0};
        do {
            if (it->type == TokenType::PAREN_OPEN) ++parenLevel;
            else if (it->type == TokenType::PAREN_CLOSE) --parenLevel;
            ++it;
        } while (parenLevel > 0);
        return it;
    }

    inline IOperation *createFunctionOperation(const std::string& name, IOperation *arg1, IOperation *arg2) {
        if (!arg1) throw parser_error(ERR_INVALID_FUNCTION + name);
        if (name == "pow" || name == "power") {
            if(!arg2)
                throw std::invalid_argument(ERR_INVALID_PARAMETERS + "pow needs two parameters but got only one");
            return new Exponentiation(arg1, arg2);
        }
        else if (name == "mod") {
            if(!arg2)
                throw std::invalid_argument(ERR_INVALID_PARAMETERS + "mod needs two parameters but got only one");
            return new Modulo(arg1, arg2);
        }
        else if (name == "log2")
            return new Log2(arg1);
        else if (name == "log10")
            return new Log10(arg1);
        else if (name == "ln")
            return new NaturalLogarithm(arg1);
        else if (name == "sin")
            return new Sin(arg1);
        else if (name == "cos")
            return new Cos(arg1);
        else if (name == "tan")
            return new Tan(arg1);
        else if (name == "abs")
            return new Abs(arg1);
        else if (name == "floor")
            return new Floor(arg1);
        else if (name == "ceil")
            return new Ceil(arg1);
        else if (name == "round")
            return new Round(arg1);
        else if (name == "sqrt")
            return new SquareRoot(arg1);
        else
            throw parser_error(ERR_PARSE_FAIL);
    }

    //Wrapper to ensure we don't recurse too deep
    template <typename It>
    IOperation *parseEquation(It tokBegin, It tokEnd) {
        volatile static size_t depth{0};
        ++depth;
        if (depth > MAX_RECURSION_DEPTH)
            throw parser_error(ERR_TOO_COMPLEX);
        IOperation *temp{_parseEquation(tokBegin, tokEnd)};
        --depth;
        return temp;
    }

    template <typename It>
    IOperation *_parseEquation(It tokBegin, It tokEnd) {
        if (tokBegin == tokEnd)
            throw parser_error(ERR_PARSE_FAIL);
        const size_t maxParenLevel{countParens(tokBegin, tokEnd)};
        for (size_t parenLevel{0}; parenLevel <= maxParenLevel; ++parenLevel) {
            for (PEState state{ADD_SUB}; state < RESET; ++state) {
                size_t accessLevel{0};

                auto it{tokEnd};
                do {
                    --it;
                    Token ct{*it};
                    if (ct.type == TokenType::PAREN_OPEN) --accessLevel;
                    else if (ct.type == TokenType::PAREN_CLOSE) ++accessLevel;
                    if (accessLevel > parenLevel) continue;

                    if (ct.type == TokenType::OPERATOR) {
                        if (state == ADD_SUB) {
                            if (it != tokBegin && prev(it)->type != TokenType::PAREN_OPEN) {
                                if (ct.value[0] == '+')
                                    return new Addition(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                                if (ct.value[0] == '-')
                                    return new Subtraction(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                            }
                        } else if (state == MUL_DIV) {
                            if (ct.value[0] == '*')
                                return new Multiplication(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                            if (ct.value[0] == '/')
                                return new Division(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                            if (ct.value[0] == '|')
                                return new BinaryOR(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                            if (ct.value[0] == '&')
                                return new BinaryAND(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                            if (ct.value[0] == '^')
                                return new BinaryXOR(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                        } else if (state == JUXTAPOS_MOD) {
                            if (ct.value[0] == '%')
                                return new Modulo(parseEquation(tokBegin, it), parseEquation(next(it), tokEnd));
                        } else if (state == UNARY) {
                            if (next(it) != tokEnd) {
                                if (ct.value[0] == '-' &&
                                    (it == tokBegin || prev(it)->value[0] == '('))
                                    return new UnaryNegative(parseEquation(next(it), tokEnd));
                                if (ct.value[0] == '~')
                                    return new BinaryNOT(parseEquation(next(it), tokEnd));
                            }
                        }
                    } else if (ct.type == TokenType::PAREN_OPEN && state == JUXTAPOS_MOD) {
                        if (it != tokBegin && prev(it)->type == TokenType::PAREN_CLOSE)
                            return new Multiplication(parseEquation(tokBegin, it), parseEquation(it, tokEnd));
                    } else if (ct.type == TokenType::PAREN_CLOSE && state == JUXTAPOS_MOD &&
                               next(it) != tokEnd) {
                        auto right{next(it)};
                        if (right->type == TokenType::NUMBER_LITERAL)
                            return new Multiplication(new Literal(std::stold(right->value)), parseEquation(tokBegin, right));
                        if (right->type == TokenType::VARIABLE)
                            return new Multiplication(new Variable(right->value[0]), parseEquation(tokBegin, right));
                    } else if (ct.type == TokenType::NUMBER_LITERAL) {
                        if (state == LITERAL) return new Literal(std::stold(ct.value));
                        if (state == JUXTAPOS_MOD && next(it) != tokEnd) {
                            auto right{next(it)};
                            if (right->type == TokenType::VARIABLE)
                                return new Multiplication(parseEquation(tokBegin, right), new Variable(right->value[0]));
                            if (right->type == TokenType::PAREN_OPEN)
                                return new Multiplication(parseEquation(tokBegin, right), parseEquation(right, tokEnd));
                        }
                    } else if (ct.type == TokenType::VARIABLE) {
                        if (state == LITERAL)
                            return new Variable(ct.value[0]);
                        if (state == JUXTAPOS_MOD && next(it) != tokEnd) {
                            auto right{next(it)};
                            if (right->type == TokenType::NUMBER_LITERAL)
                                return new Multiplication(parseEquation(tokBegin, right), new Literal(std::stold(right->value)));
                            if (right->type == TokenType::PAREN_OPEN)
                                return new Multiplication(parseEquation(tokBegin, right), parseEquation(right, tokEnd));
                            if (right->type == TokenType::VARIABLE)
                                return new Multiplication(parseEquation(tokBegin, right), new Variable(right->value[0]));
                        }
                    } else if (ct.type == TokenType::FUNCTION && state == FUN) {
                        auto argListBegin{next(it)};
                        auto argListEnd{findCorrespondingCloseParen(argListBegin)};
                        ++argListBegin;

                        IOperation *arg1{nullptr};
                        IOperation *arg2{nullptr};
                        auto comma{std::find_if(argListBegin, argListEnd, [](const Token& elem) -> bool {
                            return elem == TokenType::COMMA;
                        })};
                        if (comma == argListEnd) {
                            arg1 = parseEquation(argListBegin, argListEnd);
                        } else {
                            arg1 = parseEquation(argListBegin, comma);
                            arg2 = parseEquation(next(comma), argListEnd);
                        }

                        return createFunctionOperation(ct.value, arg1, arg2);
                    }
                } while (it != tokBegin);
            }//for(iterate over states)
        }//for(iterate over levels)
        throw parser_error(ERR_PARSE_FAIL);
    }
}