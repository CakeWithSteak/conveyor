#pragma once
#include "Operations.h"
#include "tokenizer.h"
#include <exception>
#include <stdexcept>
#include <string>

//The parser is recursive, thus a too complicated equation could cause a stack overflow
//To keep that from happening we use a macro to limit the recursion depth of the parser
#ifndef MAX_RECURSION_DEPTH
#define MAX_RECURSION_DEPTH 2000
#endif

namespace Operations {

    class parser_error : public std::runtime_error {
    public:
        explicit parser_error(const std::string& str) : std::runtime_error(str.c_str()) {}

        explicit parser_error(char *str) : std::runtime_error(str) {}
    };

    const std::string ERR_INVALID_FUNCTION{"The following function couldn't be parsed: "};
    const std::string ERR_PARSE_FAIL{"Failed to parse equation"};
    const std::string ERR_TOO_COMPLEX{"The equation is too complex, parser got stack overflow"};

    OperationTree parseEquation(TokenList&& tokens);

    inline OperationTree parseEquation(std::string& eq) {
        return parseEquation(tokenize(eq));
    }

}