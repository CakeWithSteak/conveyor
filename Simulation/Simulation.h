#pragma once
#include "Vector.h"
#include "VectorField.h"
#include <vector>
#include <utility>


//Tools for simulating some point-like objects moving along a vector field
//The vector field is divided into 1x1 squares. An object inside one of these squares would be accelerated by the vector
// in the top-left corner of the square.

//A point-like object with a position and velocity
struct PhysicsObject {
    Vector pos{0,0};
    Vector velocity{0,0};
};

class Simulation {
    std::vector<PhysicsObject> objs{};
    double maxSpeed{0};
public:
    VectorField field{};

    Simulation() = default;
    Simulation(Simulation& other) = default;
    Simulation(Simulation&& other) noexcept : objs(std::move(other.objs)), maxSpeed(other.maxSpeed), field(std::move(other.field)) {}
    explicit Simulation(VectorField& field, double maxSpeed = 0) : maxSpeed(maxSpeed), field(field) {}
    explicit Simulation(VectorField&& field, double maxSpeed = 0) : maxSpeed(maxSpeed), field(std::move(field)) {}

    Simulation& operator=(Simulation&& other) noexcept {
        objs = std::move(other.objs);
        maxSpeed = other.maxSpeed;
        field = std::move(other.field);
        return *this;
    }

    Simulation(std::vector<PhysicsObject> &&objs, VectorField&& field, double maxSpeed = 0) :
        objs(std::move(objs)), maxSpeed(maxSpeed), field(std::move(field)) {}

    //Get a read-only array of objects
    const std::vector<PhysicsObject>& getObjects() const;

    inline PhysicsObject& getObject(int64_t i) {
        return objs[i];
    }

    inline int64_t getNumObjects() const {
        return objs.size();
    }

    void pushObject(const PhysicsObject& obj);

    //Updates the position of every object in the sim
    void step(double speed = 1.0);
};


