#pragma once
#include <vector>  // This naming choice may be a bit confusing later
#include "Vector.h"
#include <cassert>
#include <stdexcept>

//Represents a field of vectors
//Coordinates are non-negative integers, the origin is in the top-left corner
//Each vector is independent and the their magnitudes are unconstrained

class VectorField {
    std::vector<Vector> data{};
    int64_t rows{0};
    int64_t cols{0};
public:
    VectorField() = default;
    VectorField(int64_t rows, int64_t cols) : data(rows * cols, Vector()), rows(rows), cols(cols) {};

    VectorField(std::vector<Vector> data, int64_t rows, int64_t cols) : data(std::move(data)), rows(rows), cols(cols) {
        assert(data.size() == static_cast<size_t>(rows * cols) && "[VectorField] Size mismatch between provided dimensions and data.");
    }


    VectorField(VectorField& vf) = default;

    VectorField(VectorField&& vf) noexcept : data(std::move(vf.data)), rows(vf.rows), cols(vf.cols) {}

    VectorField& operator=(VectorField&& other) noexcept {
        data = std::move(other.data);
        rows = other.rows;
        cols = other.cols;
        return *this;
    }

    const std::vector<Vector>& getData() const {
        return data;
    }

    int64_t getRows() const {
        return rows;
    }

    int64_t getCols() const {
        return cols;
    }

    //Element access
    Vector& operator()(int64_t x, int64_t y) {
        if(x >= cols || y >= rows) {
            throw std::out_of_range("[VectorField()] Provided coordinates are outside the vector field");
        }
        return data[y * cols + x];
    }

    const Vector& operator()(int64_t x, int64_t y) const {
        if(x >= cols || y >= rows) {
            throw std::out_of_range("[VectorField()] Provided coordinates are outside the vector field");
        }
        return data[y * cols + x];
    }
};


