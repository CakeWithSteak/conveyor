#include "Simulation.h"
#include <algorithm>
#include <omp.h>

void Simulation::pushObject(const PhysicsObject& obj) {
    objs.push_back(obj);
}



void Simulation::step(double speed) {
#pragma omp parallel for
    for(int64_t i = 0;i < objs.size();++i) {
        auto& obj = objs[i];
        const Vector& vec = field(static_cast<size_t>(obj.pos.x), static_cast<size_t>(obj.pos.y));
        obj.velocity += vec * speed;
        if(maxSpeed > 0) {
            obj.velocity.x = std::max(-maxSpeed, std::min(maxSpeed, obj.velocity.x));
            obj.velocity.y = std::max(-maxSpeed, std::min(maxSpeed, obj.velocity.y));
        }
        obj.pos += obj.velocity * speed;

        //If the object is out of bounds, loop it around
        //fixme The program will crash if an object moves outside the field by more than its width
        // If we can keep that from happening with the speed limit this should be fine
        if(obj.pos.x >= field.getCols())
            obj.pos.x -= field.getCols();
        else if(obj.pos.x < 0)
            obj.pos.x += field.getCols();
        if(obj.pos.y >= field.getRows())
            obj.pos.y -= field.getRows();
        else if(obj.pos.y < 0)
            obj.pos.y += field.getRows();
    }

}

const std::vector<PhysicsObject>& Simulation::getObjects() const {
    return objs;
}
