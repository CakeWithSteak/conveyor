#pragma once
#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#include <math.h>
#endif
#include <cmath>

//A basic vector class

class Vector {
public:
    double x{0};
    double y{0};

    Vector() = default;
    Vector(double x, double y) : x(x), y(y) {}

    //Creates a vector from polar coordinates
    static Vector fromPolar(double angle, double r) {
        return Vector(std::cos(angle) * r, std::sin(angle) * r);
    }

    double getMangnitude() const {
        return std::sqrt(x*x + y*y);
    }

    Vector& operator-() {
        x *= -1;
        y *= -1;
        return *this;
    }

    Vector& operator+=(const Vector& v) {
        x += v.x;
        y += v.y;
        return *this;
    }
    Vector& operator-=(const Vector& v) {
        x -= v.x;
        y -= v.y;
        return *this;
    }
    Vector& operator*=(double n) {
        x *= n;
        y *= n;
        return *this;
    }

    friend Vector operator+(const Vector& v1, const Vector& v2) {
        Vector res = v1;
        res += v2;
        return res;
    }
    friend Vector operator-(const Vector& v1, const Vector& v2) {
        Vector res = v1;
        res -= v2;
        return res;
    }
    friend Vector operator*(const Vector& v1, double n) {
        Vector res = v1;
        res *= n;
        return res;
    }

    bool operator==(const Vector& rhs) const {
        return x == rhs.x &&
               y == rhs.y;
    }

    bool operator!=(const Vector& rhs) const {
        return !(rhs == *this);
    }

    //Returns the angle in radians between this vector and the i-hat unit vector
    //Gives an angle in the interval [0, 2pi[
    double getAngle() const {
        double normX = x / getMangnitude();
        double angle = std::acos(normX);
        if(y < 0)
            angle = 2 * M_PI - angle;
        return angle;
    }
};


